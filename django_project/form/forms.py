from django import forms

class StatusForm(forms.Form):
    name = forms.CharField(max_length=30)
    status = forms.CharField(max_length=255)
