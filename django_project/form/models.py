from django.db import models

# Create your models here.

class StatusFormModels(models.Model):
    name = models.CharField(max_length=30)
    status = models.CharField(max_length=255)

    published = models.DateTimeField(auto_now_add=True)

    def __str__ (self):
        return "{}".format(self.name)