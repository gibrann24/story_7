from django.contrib import admin
from django.urls import path


from . import views

urlpatterns = [
    path('create/',views.create, name='create'),
    path('form/', views.form, name='form'),
    path('confirm/', views.confirm, name='confirm'),
    path('',views.index),

]
