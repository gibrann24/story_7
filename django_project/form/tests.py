from django.test import TestCase
from django.urls import resolve
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
import unittest
from .models import *
from .views import *
from .forms import *


# Create your tests here.

class story7UnitTesting(TestCase):
    def test_homepage_exist(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)
    def test_homepage_uses_indexhtml(self):
        response = self.client.get("/")
        self.assertTemplateUsed(response, "form/index.html")
    def test_homepage_uses_index_func(self):
        found = resolve("/")
        self.assertEqual(found.func, index)
    def test_confirm_page_exitst(self):
        response = self.client.get("/confirm/")
        self.assertEqual(response.status_code, 302)
    def test_confirm_page_uses_createhtml(self):
        response = self.client.get("/confirm/")
        self.assertTemplateUsed('create.html')
    def test_confirm_page_uses_confirm_func(self):
        found = resolve("/confirm/")
        self.assertEqual(found.func, confirm)
    #def test_index_page_can_post_status(self):
        response = self.client.post("/", {'name':'test_name','status':'test_status'})
        count = StatusFormModels.objects.all().count()
        self.assertEqual(1, count)
    def test_index_page_can_post_status_invalid(self):
        response = self.client.post("/", {'name':'','status':''})
        count = StatusFormModels.objects.all().count()
        self.assertEqual(0, count)

class story7FunctionalTesting(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Chrome()
    def tearDown(self):
        browser = self.browser
        browser.quit()
    def test_index_page_has_title(self):
        browser = self.browser
        browser.get("localhost:8000")
        self.assertIn('', self.browser.title)
    def test_confirm_page_has_title(self):
        browser = self.browser
        browser.get("localhost:8000/confirm")
        self.assertIn('', self.browser.title) 
    #def test_index_post_form(self):
        browser = self.browser
        browser.get("localhost:8000")
        name = browser.find_element_by_id('name')
        status = browser.find_element_by_id('status')
        submit = browser.find_element_by_id('submit')
        name.send_keys('test_name')
        status.send_keys('test_message')
        submit.send_keys(Keys.RETURN)
    #def test_index_post_invalid_form(self):
        browser = self.browser
        browser.get("localhost:8000")
        name = browser.find_element_by_name('name')
        status = browser.find_element_by_name('status')
        submit = browser.find_element_by_name('submit')
        name.send_keys('')
        status.send_keys('')
        submit.send_keys(Keys.RETURN)
    #def testConfirmPostForm(self):
        browser = self.browser
        browser.get('localhost:8000/confirmation/')
        name = browser.find_element_by_name('name')
        status = browser.find_element_by_name('status')
        submit = browser.find_element_by_name('submit')
        name.send_keys('test_name')
        status.send_keys('test_message')
        submit.send_keys(Keys.RETURN)
        submit = browser.find_element_by_id('submit')
        pageSource = browser.page_source
        self.assertIn('test_message', pageSource)

    
    

    



    

    
    

