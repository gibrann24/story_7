from django.shortcuts import render

def index(request):
    context = {
        'heading':'Homepage',

    }
    return render(request, 'index.html', context)